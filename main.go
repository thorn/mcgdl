package main

import (
	"encoding/json"
	"fmt"
    "io"
	"io/ioutil"
    "net/http"
	"os"
)

// Versions struct which contains an array of versions
type Versions struct {
	Versions []Version `json:"versions"`
}

// Version struct which contains the update
// name and the download link for client and json
type Version struct {
	Update string `json:"update"`
	Jar string `json:"jar"`
	Json string `json:"json"`
}

func main() {
	// Asks which update you want.
	fmt.Println(`What Update would you like?
    [0] 1.0.0
    [1] 1.1
    [2] 1.2.1
    [3] 1.2.2
    [4] 1.2.3
    [5] 1.2.4
    [6] 1.2.5
    [7] 1.3.1
    [8] 1.3.2
    [9] 1.4.2
    [10] 1.4.4
    [11] 1.4.5
    [12] 1.4.6
    [13] 1.4.7
    [14] 1.5
    [15] 1.5.1
    [16] 1.5.2
    [17] 1.6.1
    [18] 1.6.2
    [19] 1.6.4
    [20] 1.7.2
    [21] 1.7.4
    [22] 1.7.5
    [23] 1.7.6
    [24] 1.7.7
    [25] 1.7.8
    [26] 1.7.9
    [27] 1.7.10
    [28] 1.8
    [29] 1.8.1
    [30] 1.8.2
    [31] 1.8.3
    [32] 1.8.4
    [33] 1.8.5
    [34] 1.8.6
    [35] 1.8.7
    [36] 1.8.8
    [37] 1.8.9
    [38] 1.9
    [39] 1.9.1
    [40] 1.9.2
    [41] 1.9.3
    [42] 1.9.4
    [43] 1.10
    [44] 1.10.1
    [45] 1.10.2
    [46] 1.11
    [47] 1.11.1
    [48] 1.11.2
    [49] 1.12
    [50] 1.12.1
    [51] 1.12.2
    [52] 1.13
    [53] 1.13.1
    [54] 1.13.2
    [55] 1.14
    [56] 1.14.1
    [57] 1.14.2
    [58] 1.14.3
    [59] 1.14.4
    [60] 1.15
    [61] 1.15.1
    [62] 1.15.2
    [63] 1.16
    [64] 1.16.1
    [65] 1.16.2
    [66] 1.16.3
    [67] 1.16.4
    [68] 1.16.5`)
	var askupdate int
	fmt.Scanln(&askupdate)

	// Open the jsonFile
	jsonFile, err := os.Open("versions.json")

	// if os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	} else {
        fmt.Println("Opened versions.json")
    }

	// defer the closing of the jsonFile so that it can be parsed later
	defer jsonFile.Close()

	// read versions.json as a byte array
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]interface{}
	json.Unmarshal([]byte(byteValue), &result)

	// Initialize Versions array
	var versions Versions

	// unmarshal the byteArray which contains the
	// content of versions.json into 'versions'
	json.Unmarshal(byteValue, &versions)

	if askupdate != 8675309 {
		fileUrlJar := versions.Versions[askupdate].Jar
		err = DownloadFile(versions.Versions[askupdate].Update + "-client.jar", fileUrlJar)
		if err != nil {
			fmt.Println(err)
		}

		fileUrlJson := versions.Versions[askupdate].Json
		err = DownloadFile(versions.Versions[askupdate].Update + ".json", fileUrlJson)
		if err != nil {
			fmt.Println(err)
		}
	}

	if askupdate == 8675309 {
		fmt.Println("Are you sure you want to download ALL files available in versions.json? This will take up 600MB [Yes/No]")
		var confirm string
		fmt.Scanln(&confirm)

		if confirm == "Yes" {
			for i := 0; i < 68; i++ {
				fileUrlJar := versions.Versions[i].Jar
				err = DownloadFile(versions.Versions[i].Update + "-client.jar", fileUrlJar)
				if err != nil {
					fmt.Println(err)
				}

				fileUrlJson := versions.Versions[i].Json
				err = DownloadFile(versions.Versions[i].Update + ".json", fileUrlJson)
				if err != nil {
					fmt.Println(err)
				}
			}
		} else {
			fmt.Println("Program stopped (Chose not to download all)")
		}
	}

}

func DownloadFile(filepath string, url string) error {
    // Get the data
    resp, err := http.Get(url)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Create the file
    out, err := os.Create(filepath)
    if err != nil {
        return err
    }
    defer out.Close()

    // Write body to file
    _, err = io.Copy(out, resp.Body)
    return err
}
